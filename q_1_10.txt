B-001-001-001 (B)
Authority to make regulations governing radiocommunications is derived from:
A the ITU Radio Regulations
B the Radiocommunication Act
C the Radiocommunication Regulations
D the Standards for the Operation of Radio Stations in the Amateur Radio Service

B-001-001-002 (A)
Authority to make "Standards for the Operation of Radio Stations in the Amateur Radio Service" is derived from:
A the Radiocommunication Act
B the Radiocommunication Regulations
C the Standards for the Operation of Radio Stations in the Amateur Radio Service
D the ITU Radio Regulations

B-001-001-003 (A)
The Department that is responsible for the administration of the Radiocommunication Act is:
A Innovation, Science and Economic Development Canada
B Transport Canada
C Communications Canada
D National Defence

B-001-001-004 (B)
The "amateur radio service" is defined in:
A the FCC's Part 97 rules
B the Radiocommunication Regulations
C the Radiocommunication Act
D the Standards for the Operation of Radio Stations in the Amateur Radio Service

B-001-002-001 (A)
What must you do to notify your mailing address changes ?
A Contact Innovation, Science and Economic Development Canada and provide details of your address change
B Telephone your local club, and give them your new address
C Contact an accredited examiner and provide details of your address change
D Write amateur organizations advising them of your new address, enclosing your certificate

B-001-002-002 (C)
An Amateur Radio Operator Certificate is valid for:
A three years
B one year
C life
D five years

B-001-002-003 (A)
Whenever a change of address is made:
A Innovation, Science and Economic Development Canada must be advised of any change in postal address
B Innovation, Science and Economic Development Canada must be notified within 14 days of operation at the new address
C the station shall not be operated until a change of address card is forwarded to Innovation, Science and Economic Development Canada
D within the same province, there is no need to notify Innovation, Science and Economic Development Canada

B-001-002-004 (A)
The Amateur Radio Operator Certificate:
A must be retained at the station
B must be put on file
C must be kept in a safe place
D must be kept on the person to whom it is issued

B-001-002-005 (A)
The holder of an Amateur Radio Operator Certificate shall, at the request of a duly appointed radio inspector, produce the certificate, or a copy thereof, to the inspector, within ____ hours after the request:
A 48
B 12
C 24
D 72

B-001-002-006 (B)
The fee for an Amateur Radio Operator Certificate is:
A $24
B free
C $32
D $10

B-001-002-007 (A)
The Amateur Radio Operator Certificate should be:
A retained at the address provided to Innovation, Science and Economic Development Canada
B retained in a safety deposit box
C retained on the radio amateur's person
D retained in the radio amateur's vehicle

B-001-003-001 (A)
Out of amateur band transmissions:
A are prohibited - penalties could be assessed to the control operator
B must be identified with your call sign
C are permitted
D are permitted for short tests only

B-001-003-002 (D)
If an amateur pretends there is an emergency and transmits the word "MAYDAY," what is this called?
A A traditional greeting in May
B An emergency test transmission
C Nothing special: "MAYDAY" has no meaning in an emergency
D False or deceptive signals

B-001-003-003 (D)
A person found guilty of transmitting a false or fraudulent distress signal, or interfering with, or obstructing any radio communication, without lawful cause, may be liable, on summary conviction, to a penalty of:
A a fine of $10 000
B a prison term of two years
C a fine of $1 000
D a fine, not exceeding $5 000, or a prison term of one year, or both

B-001-003-004 (A)
What government document states the offences and penalties for non compliance of the rules governing radiocommunications?
A The Radiocommunication Act
B The Official Radio Rules of Canada
C The Radiocommunications Regulations
D The Radiocommunications Law Reform Act of 2002

B-001-003-005 (B)
Which of the following is not correct? The Minister may suspend an Amateur Radio Operator Certificate:
A Where the holder has failed to comply with a request to pay fees or interest due
B With no notice, or opportunity to make representation thereto
C Where the holder has contravened the Radiocommunication Act, its Regulations, or the terms and conditions of the certificate
D Where the certificate was obtained through misrepresentation

B-001-003-006 (A)
Which of the following statements is not correct?
A A radio inspector may enter a dwelling without the consent of the occupant and without a warrant
B Where entry is refused, and is necessary to perform his duties under the Act, a radio inspector may obtain a warrant
C In executing a warrant, a radio inspector shall not use force, unless accompanied by a peace officer, and force is authorized
D The person in charge of a place entered by a radio inspector shall give the inspector information that the inspector requests

B-001-004-001 (B)
What age must you be to hold an Amateur Radio Operator Certificate with Basic Qualification?
A 14 years or older
B There are no age limits
C 70 years or younger
D 18 years or older

B-001-004-002 (B)
Which examination must be passed before an Amateur Radio Operator Certificate is issued?
A Advanced
B Basic
C Personality test
D Morse code

B-001-004-003 (D)
Holders of which one of the following certificates may be issued an Amateur Radio Operator Certificate?
A Canadian Restricted Operator Certificate - Maritime (ROC-M)
B Canadian Restricted Operator's Certificate - Maritime Commercial (ROC- MC)
C Canadian Restricted Operator Certificate - Aeronautical (ROC-A)
D Canadian Radiocommunication Operator General Certificate Maritime (RGMC)

B-001-004-004 (B)
After an Amateur Radio Operator Certificate with Basic qualifications is issued, the holder may be examined for additional qualifications in the following order:
A Advanced after passing Morse code
B any order
C Morse code after passing the Advanced
D Morse code after passing the Basic with Honours

B-001-004-005 (D)
One Morse code qualification is available for the Amateur Radio Operator Certificate. It is:
A 12 w.p.m.
B 7 w.p.m.
C 15 w.p.m.
D 5 w.p.m.

B-001-004-006 (B)
The holder of an Amateur Radio Operator Certificate with the Basic Qualification is authorized to operate following stations:
A any authorized station except stations authorized in the amateur, aeronautical or maritime services
B a station authorized in the amateur service
C a station authorized in the aeronautical service
D a station authorized in the maritime service

B-001-004-007 (A)
What conditions must candidates to amateur radio certification meet?
A Have a valid address in Canada
B Be a Canadian citizen
C Be a Canadian citizen or permanent resident
D Be at least 14 years of age and a Canadian citizen or permanent resident

B-001-005-001 (B)
Radio apparatus may be installed, placed in operation, repaired or maintained by the holder of an Amateur Radio Operator Certificate with Advanced Qualification on behalf of another person:
A if the transmitter of a station, for which a radio authorization is to be applied for, is type approved and crystal controlled
B if the other person is the holder of an Amateur Radio Operator Certificate to operate in the amateur radio service
C pending the granting of a radio authorization, if the apparatus covers the amateur and commercial frequency bands
D pending the granting of an Amateur Radio Operator Certificate if the apparatus covers the amateur frequency bands only

B-001-005-002 (D)
The holder of an Amateur Radio Operator Certificate may design and build from scratch transmitting equipment for use in the amateur radio service provided that person has the:
A Basic and Morse code qualification
B Morse code with Honours qualification
C Basic qualification
D Advanced qualification

B-001-005-003 (D)
Where a friend is not the holder of any type of radio operator certificate, you, as a holder of an Amateur Radio Operator Certificate with Basic Qualification, may, on behalf of your friend:
A install an amateur station, but not operate or permit the operation of the apparatus
B install and operate the radio apparatus, using your own call sign
C modify and repair the radio apparatus but not install it
D not install, place in operation, modify, repair, maintain, or permit the operation of the radio apparatus

B-001-005-004 (D)
A radio amateur with Basic and Morse code qualifications may install an amateur station for another person:
A only if the final power input does not exceed 100 watts
B only if the station is for use on one of the VHF bands
C only if the DC power input to the final stage does not exceed 200 watts
D only if the other person is the holder of a valid Amateur Radio Operator Certificate

B-001-006-001 (C)
An amateur radio station with a maximum input power to the final stage of 2 watts:
A need not be licensed in isolated areas only
B is exempt from regulatory control by Innovation, Science and Economic Development Canada
C must be operated by a person with an Amateur Certificate and call sign
D must be licensed by Innovation, Science and Economic Development Canada

B-001-006-002 (C)
An amateur station may be used to communicate with:
A armed forces stations during special contests and training exercises
B any station transmitting in the amateur bands
C stations operated under similar authorizations
D any stations which are identified for special contests

B-001-006-003 (B)
Which of the following statements is not correct?
A A radio amateur may not operate, or permit to be operated, a radio apparatus which he knows is not performing to the Radiocommunication Regulations
B A radio amateur may use a linear amplifier to amplify the output of a licence-exempt transmitter outside any amateur radio allocations
C A considerate operator does not transmit unnecessary signals
D A courteous operator refrains from using offensive language

B-001-006-004 (D)
Which of the following statements is not correct?
A Except for a certified radio amateur operating within authorized amateur radio allocations, no person shall possess or operate any device for the purpose of amplifying the output power of a licence- exempt radio apparatus
B A person may operate or permit the operation of radio apparatus only where the apparatus is maintained to the Radiocommunication Regulations tolerances
C A person may operate an amateur radio station when the person complies with the Standards for the Operation of Radio Stations in the Amateur Radio Service
D An amateur radio operator transmitting unnecessary or offensive signals does not violate accepted practice

B-001-006-005 (A)
Which of the following statements is not correct? A person may operate radio apparatus, authorized in the amateur service:
A on aeronautical, marine or land mobile frequencies
B only where the person complies with the Standards for the Operation of Radio Stations in the Amateur Radio Service
C only where the apparatus is maintained within the performance standards set by Innovation, Science and Economic Development Canada regulations and policies
D except for the amplification of the output power of licence-exempt radio apparatus operating outside authorized amateur radio service allocations

B-001-006-006 (B)
Some VHF and UHF FM radios purchased for use in the amateur service can also be programmed to communicate on frequencies used for the land mobile service. Under what conditions is this permissible?
A The equipment is used in remote areas north of 60 degrees latitude
B The radio is certified under the proper Radio Standard Specification for use in Canada and licensed by Innovation, Science and Economic Development Canada on the specified frequencies
C The radio operator has a Restricted Operator's Certificate
D The equipment has a RF power output of 2 watts or less

B-001-007-001 (A)
Which of the following cannot be discussed on an amateur club net?
A Business planning
B Recreation planning
C Code practice planning
D Emergency planning

B-001-007-002 (C)
When is a radio amateur allowed to broadcast information to the general public?
A Only when broadcasts last less than 1 hour
B Only when broadcasts last longer than 15 minutes
C Never
D Only when the operator is being paid

B-001-007-003 (C)
When may false or deceptive amateur signals or communications be transmitted?
A When playing a harmless "practical joke"
B When you need to hide the meaning of a message for secrecy
C Never
D When operating a beacon transmitter in a "fox hunt" exercise

B-001-007-004 (B)
Which of the following one-way communications may not be transmitted in the amateur service?
A Morse code practice
B Broadcasts intended for the general public
C Radio control commands to model craft
D Brief transmissions to make adjustments to the station

B-001-007-005 (C)
You wish to develop and use a new digital encoding technique to transmit data over amateur radio spectrum. Under what conditions is this permissible?
A When it is used for commercial traffic
B When it includes sending the amateur station's call sign
C When the encoding technique is published in the public domain
D When it is used for music streaming content

B-001-007-006 (B)
When may an amateur station in two-way communication transmit an encoded message?
A When transmitting above 450 MHz
B Only when the encoding or cipher is not secret
C During a declared communications emergency
D During contests

B-001-007-007 (C)
What are the restrictions on the use of abbreviations or procedural signals in the amateur service?
A They are not permitted because they obscure the meaning of a message to government monitoring stations
B Only "10 codes" are permitted
C They may be used if the signals or codes are not secret
D There are no restrictions

B-001-007-008 (A)
What should you do to keep you station from retransmitting music or signals from a non-amateur station?
A Turn down the volume of background audio
B Turn up the volume of your transmitter
C Speak closer to the microphone to increase your signal strength
D Adjust your transceiver noise blanker

B-001-007-009 (B)
The transmission of a secret code by the operator of an amateur station:
A is permitted for third-party traffic
B is not permitted
C is permitted for contests
D must be approved by Innovation, Science and Economic Development Canada

B-001-007-010 (D)
A radio amateur may be engaged in communication which include the transmission of:
A programming that originates from a broadcasting undertaking
B radiocommunication in support of industrial, business, or professional activities
C commercially recorded material
D Q signals

B-001-007-011 (A)
In the amateur radio service, business communications:
A are not permitted under any circumstance
B are permitted on some bands
C are only permitted if they are for the safety of life or immediate protection of property
D are not prohibited by regulation

B-001-008-001 (C)
Where may the holder of an Amateur Radio Operator Certificate operate an amateur radio station in Canada?
A Only at the address shown on Innovation, Science and Economic Development Canada records
B Anywhere in your call sign prefix area
C Anywhere in Canada
D Anywhere in Canada during times of emergency

B-001-008-002 (B)
Which type of station may transmit one-way communications?
A VHF station
B Beacon station
C Repeater station
D HF station

B-001-008-003 (B)
Amateur radio operators may install or operate radio apparatus:
A at the address which is on record at Innovation, Science and Economic Development Canada and in two mobiles
B at any location in Canada
C only at the address which is on record at Innovation, Science and Economic Development Canada
D at the address which is on record at Innovation, Science and Economic Development Canada and at one other location

B-001-008-004 (D)
In order to install any radio apparatus, to be used specifically for receiving and automatically retransmitting radiotelephone communications within the same frequency band, a radio amateur must hold an Amateur Radio Operator Certificate, with a minimum of:
A Basic and Morse code qualifications
B Basic qualification
C Basic with Honours qualification
D Basic and Advanced qualifications

B-001-008-005 (B)
In order to install any radio apparatus, to be used specifically for an amateur radio club station, the radio amateur must hold an Amateur Radio Operator Certificate, with a minimum of the following qualifications:
A Basic with Honours
B Basic and Advanced
C Basic, Advanced and Morse code
D Basic

B-001-008-006 (C)
In order to install or operate a transmitter or RF amplifier that is neither professionally designed nor commercially manufactured for use in the amateur service, a radio amateur must hold an Amateur Operator's Certificate, with a minimum of which qualifications?
A Basic and Morse code
B Basic, Advanced and Morse code
C Basic and Advanced
D Basic with Honours

B-001-009-001 (D)
Who is responsible for the proper operation of an amateur station?
A Only the station owner who is the holder of an Amateur Radio Operator Certificate
B The person who owns the station equipment
C Only the control operator
D Both the control operator and the station owner

B-001-009-002 (A)
If you transmit from another amateur's station, who is responsible for its proper operation?
A Both of you
B You
C The station owner, unless the station records show that you were the control operator at the time
D The station owner

B-001-009-003 (D)
What is your responsibility as a station owner?
A You must allow another amateur to operate your station upon request
B You must be present whenever the station is operated
C You must notify Innovation, Science and Economic Development Canada if another amateur acts as the control operator
D You are responsible for the proper operation of the station in accordance with the regulations

B-001-009-004 (C)
Who may be the control operator of an amateur station?
A Any person over 21 years of age with Basic and Morse code qualifications
B Any person over 21 years of age
C Any qualified amateur chosen by the station owner
D Any person over 21 years of age with a Basic Qualification

B-001-009-005 (D)
When must an amateur station have a control operator?
A A control operator is not needed
B Whenever the station receiver is operated
C Only when training another amateur
D Whenever the station is transmitting

B-001-009-006 (A)
When an amateur station is transmitting, where must its control operator be?
A At the station's control point
B Anywhere in the same building as the transmitter
C At the station's entrance, to control entry to the room
D Anywhere within 50 km of the station location

B-001-009-007 (D)
Why can't family members without qualifications transmit using your amateur station if they are alone with your equipment?
A They must not use your equipment without your permission
B They must first know how to use the right abbreviations and Q signals
C They must first know the right frequencies and emission modes for transmitting
D They must hold suitable amateur radio qualifications before they are allowed to be control operators

B-001-009-008 (B)
The owner of an amateur station may:
A permit anyone to use the station and take part in communications
B permit any person to operate the station under the supervision and in the presence of the holder of the amateur operator certificate
C permit anyone to take part in communications only if prior written permission is received from Innovation, Science and Economic Development Canada
D permit anyone to use the station without restrictions

B-001-009-009 (B)
Which of the following statements is correct?
A Any person may operate a station in the amateur radio service
B Any person may operate an amateur station under supervision, and in the presence of, a person holding appropriate qualifications
C A person, holding only Basic Qualification, may operate another station on 14.2 MHz
D Radio amateurs may permit any person to operate the station without supervision

B-001-010-001 (C)
What is a transmission called that disturbs other communications?
A Transponder Signals
B Unidentified transmissions
C Harmful interference
D Interrupted CW

B-001-010-002 (A)
When may you deliberately interfere with another station's communications?
A Never
B Only if the station is operating illegally
C Only if the station begins transmitting on a frequency you are using
D You may expect, and cause, deliberate interference because it can't be helped during crowded band conditions

B-001-010-003 (C)
If the regulations say that the amateur service is a secondary user of a frequency band, and another service is a primary user, what does this mean?
A Amateurs are only allowed to use the frequency band during emergencies
B Amateurs must increase transmitter power to overcome any interference caused by primary users
C Amateurs are allowed to use the frequency band only if they do not cause interference to primary users
D Nothing special: all users of a frequency band have equal rights to operate

B-001-010-004 (A)
What rule applies if two amateurs want to use the same frequency?
A Both station operators have an equal right to operate on the frequency
B The station operator with a lesser qualification must yield the frequency to an operator of higher qualification
C The station operator with a lower power output must yield the frequency to the station with a higher power output
D Station operators in ITU Regions 1 and 3 must yield the frequency to stations in ITU Region 2

B-001-010-005 (C)
What name is given to a form of interference that seriously degrades, obstructs or repeatedly interrupts a radiocommunication service?
A Adjacent interference
B Disruptive interference
C Harmful interference
D Intentional interference

B-001-010-006 (B)
Where interference to the reception of radiocommunications is caused by the operation of an amateur station:
A the amateur station operator may continue to operate and the necessary steps can be taken when the amateur operator can afford it
B the Minister may require that the necessary steps for the prevention of the interference be taken by the radio amateur
C the amateur station operator is not obligated to take any action
D the amateur station operator may continue to operate without restrictions

B-001-010-007 (A)
Radio amateur operation must not cause interference to other radio services operating in which of the following bands?
A 430.0 to 450.0 MHz
B 7.0 to 7.1 MHz
C 144.0 to 148.0 MHz
D 14.0 to 14.2 MHz

B-001-010-008 (A)
Radio amateur operations are not ARE NOT protected from interference caused by another service operating in which of the following frequency bands?
A 902 to 928 MHz
B 144 to 148 MHz
C 222 to 225 MHz
D 50 to 54 MHz

B-001-010-009 (C)
Which of the following is not correct? The operator of an amateur station:
A may conduct technical experiments using the station apparatus
B may make trials or tests, except if there is a possibility of interference to other stations
C may make trials or tests, even though there is a possibility of interfering with other stations
D shall not cause harmful interference to a station in another service which has primary use of that band

B-001-010-010 (D)
Which of these amateur bands may be heavily occupied by licence exempt devices?
A 3.5 to 4.0 MHz
B 430 to 450 MHz
C 135.7 to 137.8 kHz
D 902 to 928 MHz

B-001-010-011 (D)
The amateur radio service is authorized to share a portion of what Industrial Scientific Medical (ISM) band that is heavily used by licence exempt devices?
A 430 to 450 MHz
B 144 to 148 MHz
C 1240 to 1300 MHz
D 2300 to 2450 MHz

B-001-011-001 (D)
Amateur radio stations may communicate:
A with anyone who uses international Morse code
B with non amateur stations
C with any station involved in a real or simulated emergency
D only with other amateur stations

B-001-011-002 (B)
During relief operations in the days following a disaster, when may an amateur use his equipment to communicate on frequencies outside amateur bands?
A When normal communication systems are overloaded, damaged or disrupted
B Never
C When relaying messages on behalf of government agencies
D When messages are destined to agencies without amateur radio support

B-001-011-003 (B)
If you hear an unanswered distress signal on an amateur band where you do not have privileges to communicate:
A you may not offer assistance
B you should offer assistance
C you may offer assistance using international Morse code only
D you may offer assistance after contacting Innovation, Science and Economic Development Canada for permission to do so

B-001-011-004 (A)
In the amateur radio service, it is permissible to broadcast:
A radio communications required for the immediate safety of life of individuals or the immediate protection of property
B music
C commercially recorded material
D programming that originates from a broadcast undertaking

B-001-011-005 (C)
An amateur radio station in distress may:
A use any means of radiocommunication, but only on internationally recognized emergency channels
B only Morse code communications on internationally recognized emergency channels
C any means of radiocommunication
D only use radiocommunication bands for which the operator is qualified to use

B-001-011-006 (B)
During a disaster, when may an amateur station make transmissions necessary to meet essential communication needs and assist relief operations?
A Only when the local emergency net is activated
B When normal communication systems are overloaded, damaged or disrupted
C Never: only official emergency stations may transmit in a disaster
D When normal communication systems are working but are not convenient

B-001-011-007 (B)
During an emergency, what power output limitations must be observed by a station in distress?
A 200 watts PEP
B There are no limitations for a station in distress
C 1000 watts PEP during daylight hours, reduced to 200 watts PEP during the night
D 1500 watts PEP

