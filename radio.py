import random
import sqlite3
import sys

from itertools import islice
from typing import List, Tuple
from pathlib import Path

fp_mode = {
        "correct": "fp_correct",
        "confidence": "fp_confidence"
}

sp_mode = {
        "correct": "sp_correct",
        "confidence": "sp_confidence"
}


class Question:
    def __init__(self, question_id: str, question: str, options: str,
                 answer: str, category: str):
        # Question ID
        self.question_id = question_id

        # Question
        self.question = question

        # Multiple choice options (all as one string)
        self.options = options

        # Answer
        self.answer = answer

        # Category:
        # Substring of Question ID but storing separately means no string
        # operations when querying by category
        self.category = category

        # Boolean for 'did you answer correctly on the first pass through the
        # questions'.
        # 0 for False, 1 for True. -1 means not answered yet.
        self.fp_correct = -1

        # Confidence level of first pass answer
        # 0 for No idea, 1 for a Guess, 2 for Confident. -1 means not answered
        # yet.
        self.fp_confidence = -1

        # Second pass answer. See above for first pass answer
        self.sp_correct = -1

        # Confidence level of second pass answer. See above.
        self.sp_confidence = -1

    def save_answer(self, mode: dict, ans: str, conf: str) -> None:
        if mode == fp_mode:
            self.fp_confidence = self.__convert_confidence(conf)
            self.fp_correct = self.__convert_answer(ans, self.fp_confidence)
        else:
            self.sp_confidence = self.__convert_confidence(conf)
            self.sp_correct = self.__convert_answer(ans, self.sp_confidence)

    def get_answer(self, mode: dict) -> Tuple[bool, int]:
        return (self.fp_correct, self.fp_confidence) \
            if mode == fp_mode else (self.sp_correct, self.sp_confidence)

    def __convert_answer(self, ans: str, conf: int) -> bool:
        if conf == 0:
            return False
        return self.answer.lower() == ans.lower()

    def __convert_confidence(self, conf_str: str) -> int:
        conf_str = conf_str.lower()
        if conf_str == "n":
            return 0
        elif conf_str == "g":
            return 1
        elif conf_str == "c":
            return 2

    def __str__(self):
        if self.fp_correct == 0:
            fpc = "False"
        elif self.fp_correct == 1:
            fpc = "True"
        else:
            fpc = "n/a"

        if self.sp_correct == 0:
            spc = "False"
        elif self.sp_correct == 1:
            spc = "True"
        else:
            spc = "n/a"

        if self.fp_confidence == 0:
            fpco = "No idea"
        elif self.fp_confidence == 1:
            fpco = "Guess"
        elif self.fp_confidence == 2:
            fpco = "Confident"
        else:
            fpco = "n/a"

        if self.sp_confidence == 0:
            spco = "No idea"
        elif self.sp_confidence == 1:
            spco = "Guess"
        elif self.sp_confidence == 2:
            spco = "Confident"
        else:
            spco = "n/a"

        str = '''
        Question ID: {}\n
        Question: {}\n
        Options: \n{}\n
        Answer: {}\n
        Category: {}\n
        1st pass correct: {}\n
        1st pass confidence: {}\n
        2nd pass correct: {}\n
        2nd pass confidence: {}\n
        '''.format(self.question_id, self.question, self.options, self.answer,
                   self.category, fpc, fpco, spc, spco)
        return str


# Database class
class Database:
    db_name = "questions.db"

    def create(self, sql: str) -> bool:
        dbfile = Path(self.db_name)
        if dbfile.is_file():
            return False
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.execute(sql)
            conn.commit()
            cur.close()
        return True

    def insert_many(self, sql: str, data: list) -> None:
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.executemany(sql, data)
            conn.commit()
            cur.close()

    def query(self, sql: str, data: tuple) -> list:
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            if data is None:
                cur.execute(sql)
            else:
                cur.execute(sql, data)
            rows = cur.fetchall()
            cur.close()
            return rows

    def update(self, sql: str, data: tuple) -> None:
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            cur.execute(sql, data)
            conn.commit()
            cur.close()


class QuestionStorage:
    db = Database()

    # Create a Question object from a database row
    # Columns are:
    # QuestionID, Question, Responses, Answer, Category, FP Correct,
    # FP Confidence, SP Correct, SP Confidence
    def create_question(dbrow: tuple) -> Question:
        q = Question(dbrow[0], dbrow[1], dbrow[2], dbrow[3], dbrow[4])
        q.fp_correct = dbrow[5]
        q.fp_confidence = dbrow[6]
        q.sp_correct = dbrow[7]
        q.sp_confidence = dbrow[8]
        return q

    # Initialize the db: create the table. No error checking. This should fail
    # if already exists.
    def initialize(self) -> bool:
        sql = '''CREATE TABLE questions
                (question_id text primary key, question text, options text,
                 answer text, category text, fp_correct integer,
                 fp_confidence integer, sp_correct integer,
                 sp_confidence integer)'''
        return self.db.create(sql)

    # Kept for reference
    # Write the given list of questions to the database. No error checking.
    def populate_storage(self, questions: List[Question]) -> None:
        sql = '''INSERT INTO questions (question_id, question, options,
                answer, category, fp_correct, fp_confidence, sp_correct,
                sp_confidence) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'''
        self.db.insert_many(sql, questions)

    # Prints all questions matching query of answered questions
    # (-1 means unanswered)
    # 'field_name' must be either fp_correct or sp_correct fields.
    def get_answered_questions(self, mode: dict) -> List[Question]:
        sql = "SELECT * from questions WHERE {} in (0,1)".\
                format(mode["correct"])
        rows = self.db.query(sql, None)
        return [QuestionStorage.create_question(row) for row in rows]

    # Returns all unanswered questions (correct==-1) for the given field
    # 'field_name' must be either fp_correct or sp_correct
    def get_unanswered_questions(self, mode: dict) -> tuple:
        sql = "SELECT * from questions WHERE {}=?".format(mode["correct"])
        rows = self.db.query(sql, (-1,))
        # Randomize the question order
        random.shuffle(rows)
        return len(rows), [QuestionStorage.create_question(row) for row in rows]

    # Saves the user's answers to the given question to the database
    def update_question(self, question: Question, mode: dict) -> None:
        correct, confidence = question.get_answer(mode)
        sql = "UPDATE questions SET {} = ?, {} = ? WHERE question_id = ?"\
                  .format(mode["correct"], mode["confidence"])
        self.db.update(sql, (correct, confidence, question.question_id))


# Basic, non-bulletproof switching of input args
def main():
    try:
        mode = sys.argv[1]
        if mode == "fp":  # Asks unanswered questions as a first pass
            do(fp_mode)
        elif mode == "sp":  # Asks unanswered questions as a second pass
            do(sp_mode)
        elif mode == "fpansw":  # Shows answered questions from the first pass
            qs = QuestionStorage()
            questions = qs.get_answered_questions(fp_mode)
            for q in questions:
                print(q)
        elif mode == "spansw":  # Shows answered questions from the second pass
            qs = QuestionStorage()
            questions = qs.get_answered_questions(sp_mode)
            for q in questions:
                print(q)
        elif mode == "readfile":  # Converts questions.txt to questions.db
            qs = QuestionStorage()
            if not qs.initialize():
                print("Failed to create db. File already exists?")
                return
            qs.populate_storage(read_questions("questions.txt"))
        else:
            print_usage()
    except KeyboardInterrupt:
        print("\n")
    except IndexError:
        print_usage()


def print_usage():
    print("Usage:\n'fp' to get unanswered first pass questions\n\
          'sp' to get unanswered second pass questions\n\
          'fpansw' to print answered first pass questions\n\
          'spansw' to print answered second pass questions")


# Main loop for asking all the questions from the unanswered query
# Updates the question in the db if user answers it.
# 'mode' must be either fp or sp.
def do(mode: dict) -> None:
    qs = QuestionStorage()
    num, questions = qs.get_unanswered_questions(mode)
    print("{} unanswered questions remaining".format(num))
    for question in questions:
        _question = ask_question(question, mode)
        qs.update_question(_question, mode)


# Prints the question and answers. Prompts for an answer and a confidence level
def ask_question(question: Question, mode: dict) -> Question:
    print("\n" + question.question + "\n" + question.options + "\n\n")

    # Loop until an acceptable answer is given
    while True:
        ans = input("Answer (a,b,c,d, or n):")
        if ans.lower() in ["a", "b", "c", "d", "n"]:
            break
        else:
            print("Bad response. Try again: a, b, c, d, or n")

    if ans.lower() != "n":
        # Loop until an acceptable answer is given
        while True:
            conf = input("Confidence (Guess, Confident):")
            if conf.lower() in ["g", "c"]:
                break
            else:
                print("Bad response. Try again: g or c")
    else:
        conf = "n"

    question.save_answer(mode, ans, conf)
    return question


# Kept for reference.
# Reads the given file, n lines at a time
# Specify n=7 to read all 6 lines of the question plus the space in between
def get_lines_iterator(filename: str, n=7):
    with open(filename, 'r') as fp:
        while True:
            lines = list(islice(fp, n))
            if lines:
                yield lines
            else:
                break


# Kept for reference
# Read all questions from the given file.
# Questions are always formatted the same, e.g.
# B-001-001-001 (C)
# What is the answer to this question?
# A A
# B B
# C C
# D D
def read_questions(filename: str) -> List[Question]:
    questions = []
    for line in get_lines_iterator(filename):
        questions.append((line[0][0:13], line[1], fix_options(line),
                          str(line[0][-3]), line[0][0:5], -1, -1, -1, -1,))
    return questions


# Improve the formatting of the questions line.
def fix_options(line: List[str]) -> str:
    a = "a) " + line[2].split('A', 1)[1]
    b = "b) " + line[3].split('B', 1)[1]
    c = "c) " + line[4].split('C', 1)[1]
    d = "d) " + line[5].split('D', 1)[1]
    return a + b + c + d


if __name__ == "__main__":
    main()
